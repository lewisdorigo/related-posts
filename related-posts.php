<?php
/**
 * Plugin Name:       Related Posts
 * Plugin URI:        https://bitbucket.org/lewisdorigo/related-posts
 * Description:       Gets related posts by looking for posts with the same taxonomy terms.
 * Version:           1.0.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */    
    
namespace Brave;

class Related {
    private $disallowedTaxonomies = [
        'post_format',
        'product_type',
    ];
    
    private $defaultOptions = [
        "post_status" => "publish",
        "post_id" => null,
        "count" => 3,
        "post_type" => null
    ];
    
    private $post;
    private $query;
    private $options;
    private $rawOptions;
    
    public function __construct(array $options = []) {
        $this->rawOptions = $options;
        
        $this->options = array_merge($this->defaultOptions, $options);        
        $this->post = get_post($this->options["post_id"]);
            
        unset($this->options["post_id"]);
        
        if($this->post) {
            $this->options["post__not_in"] = $this->post->ID;
            
            if(!$this->options["post_type"]) {
                $this->options["post_type"] = $this->post->post_type;
            }
        }
        
        $this->buildQuery();
    }
    
    private function buildQuery() {
        $tax_query = [
            "relation" => "OR"
        ];
        
        if($this->post) {
            $taxonomies = get_object_taxonomies($post->post_type);
            
            foreach($taxonomies as $taxonomy) {
                if(in_array($taxonomy, $this->disallowedTaxonomies)) { continue; }
                $terms = wp_get_post_terms($post->ID, $taxonomy);
                
                $terms = array_map(function($term) {
                    return $term->term_id;
                }, $terms);
                
                if(!$terms) { continue; }
                
                $tax_query[] = [
                    "taxonomy" => $taxonomy,
                    "field"    => "term_id",
                    "terms"    => $terms,
                    "operator" => "IN"
                ];
            }
        }
        
        unset($taxonomies, $terms);
        
        $this->options["tax_query"] = apply_filters("Dorigo/Related/TaxQuery", $tax_query, $this->post, $this->rawOptions);
        $args = apply_filters("Dorigo/Related/Args", $this->options, $this->post, $this->rawOptions);
        
        $this->query = new \WP_Query($args);
    }
    
    public function __call($func, $args) {
        return call_user_func([$this->query, $func], $args);
    }
    
    public function __get($name) {
        return isset($this->query->{$name}) ? $this->query->{$name} : null;
    }
}
